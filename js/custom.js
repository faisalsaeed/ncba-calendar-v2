// Custom JS 

/*
 * @author: Faisal Saeed
 */
$(function SieveOfEratosthenesCached(n, cache) {
  /*success stories */
  $('#tabs-nav').owlCarousel({
    loop: false,
    margin: 0,
    stagePadding: 0,
    autoplay: false,
    nav: true,
    dots: false,
    rtl: false,
    autoplayHoverPause:true,
    responsive: {
      0: {
        items: 2,
        stagePadding: 0,
        margin: 30,
        dots: false
      },
      600: {
        items: 3,
        margin: 0,
        stagePadding: 0,
        dots: false

      },
      1000: {
        items: 5,
        stagePadding: 0,
        margin: 0

      },
      1645: {
        items: 8,
        stagePadding: 0,
        margin: 0
      }

    }
  })


  /* calendar*/
  $('.calendar').slick({
    dots: false,
    infinite: false,
    autoplay: false,
    speed: 300,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    fade: true,
    responsive: [{
        breakpoint: 1024,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          infinite: true,
          dots: false
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  });
});
